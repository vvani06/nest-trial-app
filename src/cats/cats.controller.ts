import { Controller, Get, Post, Param, Body, HttpException, HttpStatus, Put } from '@nestjs/common'
import { CreateCatDto } from 'dto/create-cat.dto'
import { CatsService } from './cats.service';
import { Cat } from 'interfaces/cat.interface';
import { ForbiddenException } from 'exceptions/forbidden.exception';

@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @Post()
  async create(@Body() createCatDto: CreateCatDto) {
    this.catsService.create(createCatDto)
  }

  @Get()
  async findAll(): Promise<Cat[]> {
    return this.catsService.findAll()
  }

  @Get(':id')
  async findOne(@Param('id') identifier) {
    return `This action return a cat with id: #${identifier}`
  }

  @Put()
  async test() {
    throw new ForbiddenException();
  }
}